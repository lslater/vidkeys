// Reference the first video element; terminate the script is there isn't one
var vid = document.querySelector("video")
if (vid==null) {
	throw new Error("No video element found")
}

/**
 * Set youtube as an excluded domain
 * TODO: Set this as an overridable default
 */
browser.storage.sync.set({excluded:["www.youtube.com"]})

// Check list of excluded domains
browser.storage.sync.get("excluded").then(function(value) {
	console.log(value.excluded)
	if (value.excluded.includes(window.location.hostname)) {
		throw new Error("Disabled for this domain")
	} else {
		enable_video_keys()
	}
})

// Enable key control over video functions
function enable_video_keys() {
	document.addEventListener("keydown", function(e) {
		switch (e.key) {
			case "j":
			case "J":
				vid.currentTime = vid.currentTime - 10
				break
			case "k":
			case "K":
				if (vid.paused) {
					vid.play()
				} else {
					vid.pause()
				}
				break
			case "l":
			case "L":
				vid.currentTime = vid.currentTime + 10
				break
			case "ArrowLeft":
				vid.currentTime = vid.currentTime - 5
				break
			case "ArrowRight":
				vid.currentTime = vid.currentTime + 5
				break
		}
	})
}
